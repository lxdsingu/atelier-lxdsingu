# TP : Calculer avec singularity
Martin Souchal
* Présenté pour la premiere fois aux JDEV 2017
* Présenté à l'APC en janvier 2018 pour la journée conteneurs.

## Présentation
prez/prez.html

## TP : créer un conteneur singularity
Générer le TP :
```bash
asciidoctor -d book -a toc singularity.adoc
```

mpi-ping.c : code MPI pour le TP.

Dockerfile : Pour générer le conteneur du TP sous Docker.

Singularity : Pour générer le conteneur du TP sous Singularity.

