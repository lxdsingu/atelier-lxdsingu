class: center, middle
<img src="lxd.png" width="200">
# LXD : machine containers pour HPC/HTC
## The container lighter-visor

Alexandre Dehne Garcia, 3 juillet 2017

alexandre.dehne-garcia@inra.fr

![Licence CC-BY-NC-SA](licence.png)
---
# Où se situe LXC dans le bestiaire des conteneurs ?

## l'extrême droite : les conteneurs de processus

* 1 seul processus : docker

## la droite modérée : les conteneurs d'application

* un applicatif métier avec plusieurs processus si besoin : singularity

## l'extrême gauche : les conteneurs machine

* l'ensemble d'un système : LXC

---
# LXC/LXD c'est pareil ?

## LXC est le moteur de conteneurisation

## LXD est le moteur de gestion des instances de LXC locales ou sur le réseau

---
# LXD

## performant : quasiment pas d'overhead

## sécurisé : cgroups, namespace, seccomp, apparmor

## cli & API Rest

## très configurable et à chaud


---
# Limitations des ressources (1/2)

## disques : 
* taille (ZFS & BTRFS)

## I/O réseau 
* entrante et sortante ou seuil pour les 2
* limitation en cas de saturation

## I/O block
* assez fin mais compliqué à configurer

---
# Limitations des ressources (2/2)

## RAM
* volume ou %
* gestion du swap, priorité entre conteneurs

## CPU
* nombre de coeurs, % (absolu ou relatif), coeurs spécifiques
* combinables
* priorités interconteneurs

---
# des fonctionnalités intéressantes (1/2)
## supporte le checkpoint/restart
## freeze/unfreeze
## imbrication de conteneurs (LXD et autres)
## migration à chaud
## basé sur des dépôts d'images, publication possible

---
# des fonctionnalités intéressantes (2/2)
## snapshots à chaud, rollback et copy
## profils de conteneurs
## "réseau de gestion"
## persistant ou non


---
# Questions
