class: center, middle
<img src="logo.svg" width="200">
# Singularity : containeurisation d'application
## MBI : Montpellier Bioinformatique pour l'Ingénérie

Alexandre Dehne Garcia, 8 Novembre 2017

alexandre.dehne-garcia@inra.fr

![Licence CC-BY-NC-SA](licence.png)

---

# Avant propos

* Cette présentation n'est qu'une introduction à Singualrity. De nombreuses fonctionnalités et subtilités ne sont pas évoquées

* La présentaiton se base sur les présentations de Singularity et LXC présentées au JDEV avec Martin Souchal (IN2P3)

---
# PLAN

## 1. Généralités sur les conteneurs

## 2. Le duel *Syngularity*   /   *Docker* 

## 3. Focus sur *Syngularity* 

---
# PLAN

## 1. Généralités sur les conteneurs
* 1.1 Un conteneur c'est quoi ?
* 1.2 Brève histoire des conteneurs
* 1.3 le bestiaire des conteneurs 

---
# 1.1 Un conteneur c'est quoi ?
## Virtualisation "light"

* Presque pas d'overhead par rapport à du baremetal

* Utilise le noyau du système hôte

* Isolation et automatisation

* "Portabilité" et "reproductibilité"  relatives

---
# 1.2 Brève histoire des conteneurs
## Lièe au noyau linux

* Existe chez *Solaris* depuis longtemps (jails)

* Apparition des namespaces dans le noyau 2.4.19

* cgroups 2.6.24

* LXC en 2008

* *Docker* apparait en 2013

* En 2015 *Docker* entre dans le top 15 GitHub

* Avril 2016, release de Singularity 1.0

* bien d'autres techno de conteneurs existent !

---
# 1.3 Où se situe Singularity dans le bestiaire des conteneurs ?

## l'extrême droite : les conteneurs de processus

* 1 seul processus : docker

## au centre : les conteneurs d'applications

* un (ou des) applicatif métier avec plusieurs processus si besoin : singularity

## l'extrême gauche : les conteneurs machine

* l'ensemble d'un système : LXC

---
# PLAN

## 1. Généralités sur les conteneurs

## 2. Le duel *Syngularity*   /   *Docker* 
* 2.1 Différences entre *Singularity* et *Docker* 
* 2.2 Avantages de *Singularity*
* 2.3 Limitations de *Singularity*

---
# 2.1 Différences entre *Singularity* et *Docker*  (1)
* Docker : micro service
* Singularity : application (ou plusieurs)
* Pas de virtualisation réseau (docker crée un bridge, puis des interfaces avec des IP privées)
* Les I/O sont redirigées dans l'environnement hôte
* Un conteneur *Singularity* est une image, contrairement à *Docker* et ses "couches"

 <img src="docker-filesystems-multilayer.png" width="200">
* Durée de vie du conteneur : exécution d'un programme (et service depuis 2.4)

---
# 2.1 Différences entre *Singularity* et *Docker* (2)
⚠️<span class="alert">Sécurité</span>⚠️

* Conteneur user-mode : pas de daemon root

* Isolation : pas d'élévation des privilèges avec *Singularity*

* *Docker* pas d’isolation : une appli root du conteneur qui s’échappe = un attaquant root sur la machine physique

* un user non root ne peut être root dans un conteneur *Singularity*

---
# 2.2 Avantages de *Singularity*

* BYOE : les utilisateurs peuvent emmener leur propre environnement dans le cloud/cluster
* Un conteneur *singularity* peut être facilement transporté (fichier à transferer)
* Compatible tous Linux, installation simple
* Compatible avec tous les scheduler
* Compatible avec *Docker* : il est très facile de transformer un conteneur *Docker* en conteneur *Singularity*
* MPI/X11 intégrés dans *Singularity*
* Pas de perte de performances entre un programme executé hors conteneur et à l'intérieur (temps d'instanciation quelques us)
* Utilisation d'infiniband possible dans le conteneur

---
# 2.3 Limitations de *Singularity*

* Compatibilité descendante problématique (un conteneur crée sous CentOs 5 peut tourner sur CentOs 5+ mais l'inverse est plus compliqué)
* Les fonctionnalités dépendantes du kernel ne seront pas portables
* Compatibilité hors linux compliquée (utilisation de *Vagrant* sous Mac/Windows)
* Repository public lié à *GitHub* uniquement : https://singularity-hub.org/
* marketplace encore jeune
* Pour openMPI et GPUs : la version dans le conteneur doit être supérieure ou égale à la version de la machine hôte
---
--- 
# PLAN

## 1. Généralités sur les conteneurs

## 2. Le duel *Syngularity*   /   *Docker*

## 3. Focus sur *Syngularity* 
* 3.1 Origine de *Singularity*
* 3.2 Grands principes de *Singularity*
* 3.3 Cas d'usage pratique dans la science
* 3.4 Reproductibilté | Répétabilité
* 3.5 Commandes de bases

---
# 3.1 Origine de *Singularity*

* Projet de Gregory M. Kurtzer (Warewulf, Centos, …)

* Issu du Berkeley Lab (US department of Energy - http://scs.lbl.gov/)

* Démarré en 2015, v1 en avril 2016, v2 en mai 2016, v2.3 en juin 2017

* v2.4 en octobre 2017 (changements et nouveautés!)

* Avril 2017 : création de *SingularityWare LLC*

---
# 3.2 Grands principes de *Singularity*

---
# 3.3 Cas d'usage pratique dans la science

* Environnement de travail python prêt à l'emploi transportable sur n'importe quel cluster de calcul
* Les données restent dans les espaces de travail et n'ont pas besoin d'être intégrées au conteneur
* Tout ça de manière sécurisée sans avoir besoin d'être root

# 3.4 Reproductibilté | Répétabilité

* Le journal [Rescience](http://rescience.github.io/) offre la possibilité de soumettre des articles scientifiques qui se doivent d'être reproductibles.
* Figer l'environnement d'execution dans un conteneur et le joindre à une publication garantit la répétabilité
* Voir le TP de Loic Gouarin : https://github.com/gouarin/container_precis2017
* Attention pas de reproductibilité avec les conteneurs mais de la répétabilité !

---
# 3.5 Commandes de bases

* build: créer ou transformer une conteneur *Singularity*
* pull: télécharger une image depuis un hub
* shell: lancer un shell dans un conteneur
* run: lancer et éxécuter une conteneur
* exec: lancer une commande dans un conteneur

<img src="singularity-2.4-flow.png" width="100%">

---

# Exemples de commandes

* Build conteneur
```bash
singularity build example.simg example.def
```
* Import depuis *Docker*
```bash
singularity build example.img docker://sysmso/openmpi
```
* Execution d'un code dans un conteneur (compilation)
```bash
singularity exec example.img mpicc mpi-ping.c -o ./mpi-ping
```
* Execution d'un programme *OpenMPI*
```bash
mpirun -np 20 singularity exec example.img ./mpi-ping
```

---
layout: false
# Bootstrap file
.left-column[
Singularity bootsrap file
```bash
BootStrap: debootstrap
OSVersion: yakkety
MirrorURL: http://us.archive.ubuntu.com/ubuntu/
Include: python wget software-properties-common build-essential python-dev sgml-base rsync xml-core openssh-client

%post

    add-apt-repository universe
    apt update
    apt -y install cmake git gfortran openmpi-common openmpi-bin libopenmpi-dev
    apt clean
```
]
.right-column[
Dockerfile
```bash
FROM ubuntu:yakkety

RUN apt update && \
    apt install -y python wget software-properties-common build-essential python-dev sgml-base rsync xml-core openssh-client && \
    apt clean

RUN add-apt-repository universe && \
    apt update && \
    apt -y install cmake git gfortran openmpi-common openmpi-bin libopenmpi-dev && \
    apt clean

ADD ./mpi-ping.c /work/mpi-ping.c
```
]

---
# le fichier de recette Singularity : recipe
## A) une zone HEADER (entête)
* choix de l'OS, quelle version ? D'où vient-il ? Quels paquets ? ...

## B) une zone SECTIONS (spécifications)
* code global 
* code par App

---
# le fichier de recette Singularity : recipe
## A) une zone HEADER (entête)
* ```Boostrap: shub```
* ```Boostrap: docker```
* ```Boostrap: localimage```
* ```Boostrap: debootstrap```
* ```Boostrap: yum```
* ...

puis en fonction de la valeur de ```Boostrap:```
* ```From: ```
* ```OSVersion: ```
* ```MirrorURL:```
* ```Include:```
* ...


---
# le fichier de recette Singularity : recipe
## A) une zone HEADER (entête)
shub
```
Bootstrap: shub
From: vsoch/hello-world
```

debootstrap
```
Bootstrap: debootstrap
OSVersion: yakkety
MirrorURL: http://us.archive.ubuntu.com/ubuntu/
Include: wget vim 
```

docker
```
BootStrap: docker
From: ubuntu:16.04
```

---
# le fichier de recette Singularity : recipe
## une zone SECTIONS (spécifications) dans l'ordre d'éxécution
code gobal
    * ```%help```
    * ```%setup``` éxécuté sur l'hôte, copier les fichiers pour %post
    * ```%post```
    * ```%files``` 
    * ```%labels```
    * ```%runscript```
    * ```%test```
    * ```%environment``` chargé au runtime
* si erreur alors fin de build

---
# le fichier de recette Singularity : recipe
## une zone SECTIONS (spécifications) dans l'ordre d'éxécution
code par app [http://singularity.lbl.gov/docs-recipes#apps]
selon la norme [http://containers-ftw.org/SCI-F/]
    * %apphelp NomApp
    * %appinstall NomApp
    * %appfiles NomApp
    * %applabels NomApp
    * %appinstall NomApp

singularity exec --app NomApp container.simg

---
# Questions
