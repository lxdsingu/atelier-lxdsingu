class: center, middle
<img src="logo.svg" width="200">
# Singularity : container for HPC/HTC

Martin Souchal, 23 janvier 2018

![Licence CC-BY-NC-SA](licence.png)
---
# Un conteneur c'est quoi ?
## Virtualisation "light"

* Peu d'overhead par rapport à une VM

* Utilise le noyau du système hôte

* Isolation et automatisation

* Portabilité et reproductibilité

---
# Brève histoire des conteneurs
## Lièe au noyau linux

* Existe chez Solaris depuis longtemps (jails)

* Apparition des namespaces dans le noyau 2.4.19

* cgroups 2.6.24

* LXC en 2008

* *Docker* apparait en 2013

* En 2015 *Docker* entre dans le top 15 GitHub

---
# Science et HPC
## En quoi les conteneurs peuvent nous aider dans le contexte scientifique ?

* Collaboration
    - Diffusion et partage simple de logiciels
    - Portabilité des logiciels
    - Technologie simple à mettre en œuvre
    - Rapidité de mise œuvre

* Reproductibilité
    - Assurance de répétabilité 
    - Assurance de retrouver le même environnement
    - Assurance de retrouver les même résultats

* Autres
    - Solution identique du laptop au meso-centre
    - Interface graphique

---
# Différences entre Singularity et Docker

* Docker : micro service
* Singularity : application
* Pas de virtualisation réseau dans Singularity (docker crée un bridge, puis des interfaces avec des IP privées)
* Les I/O sont redirigées dans l'environnement hôte dans Singularity
* Un conteneur *Singularity* est une image, contrairement à *Docker* et ses "couches"
<br /><img src="docker-filesystems-multilayer.png" width="200">
* Durée de vie du conteneur Singularity : exécution d'un programe

⚠️<span class="alert">Sécurité</span>⚠️

* Conteneur Singularity user-mode : pas de daemon root
* Isolation : pas d'élévation des privilèges avec *Singularity*
* *Docker* pas d’isolation : une appli root du conteneur qui s’échappe = un attaquant root sur la machine physique

---
# Avantages de singularity

* BYOE : les utilisateurs peuvent emmener leur propre environnement dans le cloud/cluster
* Un conteneur *singularity* peut être facilement transporté (fichier à transferer)
* Compatible tous Linux, installation simple
* Compatible avec tous les scheduler
* Compatible avec *Docker* : il est très facile de transformer un conteneur *Docker* en conteneur *Singularity*
* MPI/X11 intégrés dans *Singularity*
* Pas de perte de performances entre un programme executé hors conteneur et à l'intérieur (temps d'instanciation quelques us)
* Utilisation d'infiniband, GPU, etc possible dans le conteneur
* Possibilité d'avoir des catalogues de conteneurs public/privé

---
# Limitations

* Compatibilité descendante problématique (un conteneur crée sous CentOs 5 pour tourner sur CentOs > 5 mais l'inverse est plus compliqué)
* Les fonctionnalités dépendantes du kernel ne seront pas portables
* Compatibilité autre OS compliquée (utilisation de *Vagrant* sous Mac/Windows)
* Pas de *market*
* Durée de vie du conteneur : execution d'un programe -> pas adapté pour tache de fond (daemon, serveur, etc...)
* Pour openMPI et GPUs : la version dans le conteneur doit être supérieure ou égale à la version de la machine hôte

---
# Origine de Singularity

* Projet de Gregory M. Kurtzer (Warewulf, Centos, …)

* Issu du Berkeley Lab (US department of Energy - http://scs.lbl.gov/)

* Démarré en 2015, v1 en avril 2016, v2 en mai 2016, v2.3 en juin 2017

* Avril 2017 : création de *SingularityWare LLC*

---
# Cas d'usage pratique dans la science

* Environnement de travail python prêt à l'emploi transportable sur n'importe quel cluster de calcul
* Les données restent dans les espaces de travail et n'ont pas besoin d'être intégrées au conteneur
* Tout ça de manière sécurisée sans avoir besoin d'être root

# Reproductibilté

* Le journal [Rescience](http://rescience.github.io/) offre la possibilité de soumettre des articles scientifiques qui se doivent d'être reproductible.
* Figer l'environnement d'execution dans un conteneur et le joindre à une publication garantit la reproductibilité
* Voir le TP de Loic Gouarin : https://github.com/gouarin/container_precis2017

⚠️<span class="alert">Attention pas de reproductibilité avec les conteneurs mais de la répétabilité !</span>

---
# Création d'un conteneur Singularity
## Avant de commencer

* Conteneur en mode "sandbox"
    * Pour le déveleppement, tests
    * Possibilité de modifier le conteneur au fur et a mesure

* Conteneur en mode distribution (par défaut)
    * Il n'est plus modifiable une fois construit
    * Assurance que le destinataire du conteneur ne puisse pas altérer son contenu
    * Pour mettre a jour le conteneur, il faut l'incrémenter
---
# Commandes de bases

* build: Create the container
* exec: Execute a command to your container
* inspect: See labels, run and test scripts, and environment variables
* pull: pull an image from Docker or Singularity Hub
* run: Run your image as an executable
* shell: Shell into your image


---
# Création d'un conteneur

* Création du conteneur depuis un fichier de "recette"
```bash
singularity build example.img example.def
```
* Import depuis *Docker*
```bash
singularity import example.img docker://sysmso/openmpi
```
* Execution d'un code dans un conteneur (compilation)
```bash
singularity exec example.img mpicc mpi-ping.c -o ./mpi-ping
```
* Execution d'un programme *OpenMPI*
```bash
mpirun -np 20 singularity exec example.img ./mpi-ping
```

---
layout: false
# Bootstrap file
.left-column[
Singularity bootsrap file
```bash
Bootstrap: docker
From: ubuntu:latest

%post

    add-apt-repository universe
    apt-get update
    apt-get -y install python wget software-properties-common build-essential python-dev sgml-base rsync xml-core openssh-client cmake git gfortran openmpi-common openmpi-bin libopenmpi-dev
    apt-get clean
    exit 0

%files

    mpi-ping.c /work/mpi-ping.c

```
]
.right-column[
Dockerfile
```bash
FROM ubuntu:latest

RUN apt-get update && \
    apt-get install -y python wget software-properties-common build-essential python-dev sgml-base rsync xml-core openssh-client && \
    apt-get clean

RUN add-apt-repository universe && \
    apt-get update && \
    apt-get -y install cmake git gfortran openmpi-common openmpi-bin libopenmpi-dev && \
    apt-get clean

ADD ./mpi-ping.c /work/mpi-ping.c
```
]

---
# Questions
