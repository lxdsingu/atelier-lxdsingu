**Origine** : https://gitlab.in2p3.fr/souchal/tp-singularity

Auteurs du premier volet présenté aux JDEVs :
  - Alexandre Dehne Garcia <alexandre.dehne-garcia@inra.fr>, 
  - Martin Souchal <souchal@apc.in2p3.fr>.

Cette sous-section de TP est fait par Rémy Dernat <remy.dernat@umontpellier.fr> pour l'ANF *ust4hpc* ( https://ust4hpc.sciencesconf.org/ ).

Ce projet est disponible [ici](http://gitlab.mbb.univ-montp2.fr/lxdsingu/atelier-lxdsingu) et peut être cloné simplement avec `git` : 

```bash
git clone http://gitlab.mbb.univ-montp2.fr/lxdsingu/atelier-lxdsingu.git
# puis on construit le html avec asciidoctor
cd ust4hpc
asciidoctor -d book -a toc tpSingu.adoc
asciidoctor -d book -a toc bonnes-pratiques.adoc
```
